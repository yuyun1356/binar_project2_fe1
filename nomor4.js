//Nomor 4
//Buatlah sebuah function yang berfungsi untuk melakukan pengecekan apakah password yang diberikan sebagai parameter memenuhi kriteria yang telah ditentukan atau tidak. 

function isValidPassword (givenPassword) {

    let formatPassword = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[\w\W]{8,}/g

    if (givenPassword === undefined){
        return "ERROR : Pastikan Anda telah memasukkan password"
    } 

    if (typeof givenPassword !== "string"){
        return "ERROR : Invalid data type"
    }

    if (formatPassword.test(givenPassword)){
        return true;
    } else {
        return false;
    }
}

console.log(isValidPassword("Meong2021"))
console.log(isValidPassword("meong2021"))
console.log(isValidPassword("@eong"))
console.log(isValidPassword("Meong2"))
console.log(isValidPassword(0))
console.log(isValidPassword())