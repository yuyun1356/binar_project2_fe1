//Nomor 7
// membuat sebuah function yang berfungsi membantu Pak Aldi untuk menghitung total seluruh sepatu yang terjual.

const dataPenjualanPakAldi = [
    {
        namaProduct : "Sepatu Futsal Nike Vapor Academy 8",
        hargaSatuan :760000,
        kategori : "Sepatu Sport",
        totalTerjual : 90,
    },
    {
        namaProduct : "Sepatu Warrior Tristan Black Brown High",
        hargaSatuan :960000,
        kategori : "Sepatu Sneaker",
        totalTerjual : 37,
    },
    {
        namaProduct : "Sepatu Warrior Tristan BMaroon High",
        hargaSatuan :360000,
        kategori : "Sepatu Sneaker",
        totalTerjual : 90 ,
    },
    {
        namaProduct : "Sepatu Warrior Rainbow Tosca Corduroy",
        hargaSatuan : 120000,
        kategori : "Sepatu Sneaker",
        totalTerjual : 90,
    }
]

//Cara ke-1
function getTotalPenjualan (dataPenjualan){

    let total = dataPenjualan[0].totalTerjual + dataPenjualan[1].totalTerjual + dataPenjualan[2].totalTerjual + dataPenjualan[3].totalTerjual 
    return total;
}

console.log(getTotalPenjualan(dataPenjualanPakAldi))

//Cara ke-3
// function getTotalPenjualan (dataPenjualan){
    
//     //buat variabel kosong untuk menampung 
//     let hasil = 0;

//     //metode map untuk manipulasi isi array
//     const total = dataPenjualanPakAldi.map(function(i){
//         return i.totalTerjual;
//     })

//     //perulangan for
//     for(i=0; i<total.length; i++){
//         hasil += total[i]
//     }
//     return(hasil)
// }

// console.log(getTotalPenjualan(dataPenjualanPakAldi))



//Cara ke-2
// function getTotalPenjualan(dataPenjualan){

//     let total = 0;

//     for(i=0; i<dataPenjualanPakAldi.length; i++){
//         total += dataPenjualan[i].totalTerjual
//     }

//     return total;
// }

// console.log(getTotalPenjualan(dataPenjualanPakAldi))