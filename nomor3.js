//Nomor 3
// Buatlah sebuah function yang berfungsi untuk melakukan pengecekan apakah alamat email yang diberikan sebagai parameter, adalah alamat email yang formatnya benar atau tidak.

function checkEmail (email){

    // let formatEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let formatEmail = /^[\w-.]+@([\w-]+.)+\.+[\w-]{2,4}$/

    if (email === undefined){
        return "ERROR: Email tidak ditemukan!!"
    }
    
    if (typeof email != "string" ){
        return "ERROR: Pastikan email yang anda masukkan sudah benar"
    } 
    
    if(!/[@]/.test(email)){
        return "ERROR: Pastikan email anda menggunakan karakter @"
    } 
    
    if(formatEmail.test(email)){
        return "Valid"
    } else{
        return "Invalid"
    }
}

console.log(checkEmail("apranata@binar.co.id"));
console.log(checkEmail("apranata@binar.com"));
console.log(checkEmail("apranata@binar"));
console.log(checkEmail("apranata"));
console.log(checkEmail(3322))
console.log(checkEmail());