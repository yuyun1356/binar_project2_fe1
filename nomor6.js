//Nomor 6
//Buatlah sebuah function yang berfungsi untuk mendapatkan angka terbesar kedua dari sebuah array.

function getAngkaTerbesarKedua(dataNumbers){

    if (dataNumbers === undefined){
        return "ERROR: Pastikan kamu telah mengisi data angka"
    }

    if(!Array.isArray(dataNumbers)){
        return "invalid data type"
    }
    
    //Metode Numeric Sort()
    //the sort() method will produce incorrect result when sorting numbers, but You can fix this by providing a compare function ==> function(a, b){return a - b}
    
    if (dataNumbers.length >= 2){
        let hasil = dataNumbers.sort(function(a, b){return b - a});
        return dataNumbers[1]
    } else if (dataNumbers.length < 2){
        return "ERROR: Pastikan data yang kamu input lebih dari 1"
    }
}

const dataAngka = [9,4,7,7,4,3,2,2,8]

console.log(getAngkaTerbesarKedua(dataAngka))
console.log(getAngkaTerbesarKedua(0))
console.log(getAngkaTerbesarKedua())
console.log(getAngkaTerbesarKedua([0]))
console.log(getAngkaTerbesarKedua([10,2,-3,4]))