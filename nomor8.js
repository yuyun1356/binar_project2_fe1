//Nomor 8
//membuat sebuah function yang berfungsi membantu Ibu Daniela untuk mendapatkan informasi berupa Total Keuntungan, Total Modal, Produk Buku Terlaris, Penulis Buku Terlaris dan Persentase Keuntungan dari data penjualan yang telah disediakan diatas.

const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    },
];

function getInfoPenjualan(dataPenjualan){

    //Membuat variabel baru untuk menyimpan data
    let totalKeuntungan = 0;
    let totalModal = 0;
    let persentaseKeuntungan = 0;
    let temp = 0;
    let result = 0;

    //Membuat perulangan
    for(i=0; i<dataPenjualan.length; i++){

        //Menghitung total keuntungan
        totalKeuntungan += (dataPenjualan[i].hargaJual - dataPenjualan[i].hargaBeli) * dataPenjualan[i].totalTerjual
    
        //Menghitung total modal
        totalModal += (dataPenjualan[i].totalTerjual + dataPenjualan[i].sisaStok) * dataPenjualan[i].hargaBeli
    
        //Menghitung persentase keuntungan
        persentaseKeuntungan = Math.round((totalKeuntungan/totalModal) * 100) 

        // Mencari produk terlaris
        if(temp < dataPenjualan[i].totalTerjual ){ 
            temp = dataPenjualan[i].totalTerjual

            //simpan object ke variabel terlaris
            result = dataPenjualan[i]
      }
    }
    
    return {
        totalKeuntungan: "Rp." + totalKeuntungan.toLocaleString(), 
        totalModal: "Rp." + totalModal.toLocaleString(), 
        persentaseKeuntungan : persentaseKeuntungan.toString() + "%", 
        produkBukuTerlaris : result.namaProduk, 
        penulisTerlaris : result.penulis,
      }   
}

console.log(getInfoPenjualan(dataPenjualanNovel));
