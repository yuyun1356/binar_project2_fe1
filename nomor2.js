//Nomor 2
//Buatlah sebuah arrow function yang berfungsi mendeteksi apakah sebuah angka termasuk angka genap atau ganjil.

//menggunakan named arrow function (fungsi panah bernama), biasanya diinisialisasi ke dalam variabel, nama variabel tersebut digunakan sebagai nama fungsi.

const checkTypeNumber = (givenNumber) => {

    //percabangan if else 
    if (givenNumber === undefined){
        return "Error: Bro where is the parameter?"
    } else if (typeof givenNumber != "number"){
        return "Error: Invalid data type"
    } else if (givenNumber %2 === 0) {
        return "GENAP" 
    } else if (givenNumber %2 === 1){
        return "GANJIL"
    }
}
console.log(checkTypeNumber(10))
console.log(checkTypeNumber(3))
console.log(checkTypeNumber("3"))
console.log(checkTypeNumber({}))
console.log(checkTypeNumber([]))
console.log(checkTypeNumber())
