//Nomor 5
// Buatlah sebuah function yang berfungsi untuk membagikan sebuah nama menjadi Nama Depan, Nama Tengah, Nama Belakang

function getSplitName (personName){

    if (personName === undefined){
        return "ERROR: Pastikan Anda telah mengisi nama"
    }

    if (typeof personName !== "string"){
        return "ERROR: Pastikan Anda mengisi nama dengan benar"
    }

    //metode split(), ("") => sbg separator
    //syntax => const namaVariabel = target.split()
    const name = personName.split(" ")
    
    if(name.length > 3){
        return "ERROR: This function is only for 3 character name"
    } else if (name.length === 3){
        return {
            firstName: name[0], //dipanggil
            middleName: name[1],
            lastName: name[2],
        }
    } else if (name.length === 2){
        return {
            firstName: name[0],
            middleName: null,
            lastName: name[1],
        }
    } else if (name.length === 1){
        return {
            firstName: name[0],
            middleName: null,
            lastName: null,
        }
    } else {
        return "Invalid"
    }
}

console.log(getSplitName("Adi Daniela Pranata"))
console.log(getSplitName("Dwi Kuncoro"))
console.log(getSplitName("Aurora"))
console.log(getSplitName("Aurora Aureliya Sukma Darma"))
console.log(getSplitName(0))
console.log(getSplitName())