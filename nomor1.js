// SOAL NOMOR 1
//Buatlah sebuah function dengan nama changeWord yang berfungsi untuk menggantikan sebuah kata didalam sebuah kalimat. Function akan menerima 3 parameter (selectedText, changedText, dan text)

function changeWord(selectedText, changedText, text){

    //penggunaan metode string replace()
    //sintaks ==> replace(target, pengganti)
    let result = text.replace(selectedText, changedText);
    return result;
}

const kalimat1 = "Andini sangat mencintai kamu selamanya";
const kalimat2 = "Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu";

//pemanggilan function
console.log(changeWord("mencintai", "membenci", kalimat1));
console.log(changeWord("bromo", "semeru", kalimat2));